---
title: "Experimento de Sonar"
subtitle: 📱📢🧱
math: true
author: Martín Aramayo, Alberto Villagran
image: "/page/medir-distancias-con-trigonometria/regla-mano-izquierda-portrait.webp"
tags: ["Sonido", "Ondas", "Reflexión", "Señales", "Smartphone", "Phyphox"]
date: 2022-10-19
image: false
--- 

El experimento Sonar envía sonidos cortos a través de un parlante y graba el eco con un micrófono. Muestra un gráfico indicando donde rebotó el audio.

<!--more-->

- - -
* **Dificultad:** ★★☆☆☆

* **Materiales:** 
    * Smartphone (con la app [Phyphox](https://phyphox.org/)) 

* **Duración de preparación:** 40 min

* **Temas:** Sonido, cinemática.
- - -

La gente de Phyphox armo estos experimentos, los hacemos disponibles traducidos. Dejo link al [original](https://phyphox.org/wiki/index.php?title=Experiment:_Sonar).

![](https://phyphox.org/wp-content/uploads/2019/09/cropped-phyphox_dark_r2-1-1.png)


### Requisitos

En teoría el teléfono puede hacer este experimento por su cuenta, es casi imposible interpretar los ecos generados en una habitación cualquiera. Cada objeto en la habitación contribuye al resultado. Por lo tanto, tenés que aislar el teléfono en todas las direccione del teléfono (En el altavoz y micrófono) que no utilices.

<!-- While in theory the phone can perform this experiment on its own, it is
nearly impossible to interpret the echos generated in a random room. The
floor, the ceiling, every wall and every object will contribute to the
result. Therefore, you should shield all directions from the phone
(speaker and microphone) which you do not need. -->

Por ejemplo, esto se puede conseguir con un aislante, ropa, o almohadas. Puedes construir una cavidad en la cual se pueda colocar el teléfono dejando un lado abierto a los ecos. Por este lado podés colocar un buen reflector como una baldosa.

Como tu teléfono está en esta cavidad, quizás te convenga usar la interfaz [remota](http://phyphox.org/remote-control/) de un segundo dispositivo (PC, Tablet, Smartphone, etc.) para controlar el experimento y obtener los resultados. 

Debe haber mínimo ruido de ambiente durante el experimento. Inclusive hablar puede perturbar el experimento.

Puedes mejorar este experimento usando un micrófono y parlante externo bien direccionados y aislados.

### El arreglo

Ubica tu teléfono de tal manera que solo escuche el eco que te interesa e inicia el experimento. Asegurate de aumentar el volumen. Escucharás un click en el teléfono y deberías ver datos de distancia en el gráfico. Probablemente, todavía veas picos de ecos irrelevantes, pero si movés la superficie reflectante, verás que el pico correcto también se mueve.

### Análisis

El sonido que se escucha no es cualquier click, es conocido como [chirp](https://www.wikiwand.com/en/Chirp). Es una función senoidal que carga su frecuencia rápidamente. En este caso va desde 1 kHz a 4 kHz por un periodo superior a 5 ms. Adicionalmente, una función de pes ose agrega para que el [chirp](https://www.wikiwand.com/en/Chirp) inicie y termine de forma suave. El [chirp](https://www.wikiwand.com/en/Chirp) se repite 5 veces en un intervalo de 30 ms.

<!-- The noise you hear is not just a simple click, but a so-called chirp. A
chirp is a sine function which changes its frequency rapidly. In this
case it rises from 1 kHz to 4 kHz over a period of 5 ms. Additionally, a
weighting function is applied to allow for a smooth beginning and ending
of the chirp. The chirp is then repeated five times at an interval of
about 30 ms. -->

El grabado ocurre en simultáneo. Phyhpox calcula la correlación cruzada de los cinco [chirps](https://www.wikiwand.com/en/Chirp) y la grabación. El resultado es una medida de que tan fuerte el [chirp](https://www.wikiwand.com/en/Chirp) y la grabación coinciden en cualquier instante de tiempo. La correlación más fuerte ocurre en el inicio del experimento. Este punto se considera como el  \\(t=0\\) y el resto de la correlación cruzada se muestra al usuario. 
Para que el gráfico se vea más bonito, se lo suaviza con un filtro gaussiano (ancho \\(\sigma\\) de 3 puntos de datos).

El tiempo en el eje \\(x\\) es multiplicado por la velocidad del sonido y dividido por 2 para dar la correspondiente distancia de los ecos.

### Problemas y soluciones

* Muchos picos al azar. Esto puede ser causado por un fuerte ruido de fondo. Intentar hacer el experimento en un ambiente sin ruido.
* Muchos picos en posiciones fijas. Si aparecen siempre a la misma distancia (mientras el teléfono se encuentra en el mismo lugar), debes mejorar el aislamiento. Muy posiblemente estos picos provengan de otros objetos reflectantes.
<!-- * Many peaks in fixed positions. If many peaks appear which are always
at the same distance (while the phone remains in the same place),
you should try to improve the shielding from the environment as
these probably originate from other sound reflecting objects. -->


### Video Demostración

* Versión en inglés https://youtu.be/Ebj3v701HE0
* Versión en alemán <https://youtu.be/3JtJoJAAgKU>

### Recomendaciones para aprender más

* [Reflexión de sonido (Hyperphysics)](http://hyperphysics.phy-astr.gsu.edu/hbasees/Sound/reflec.html#c1)
<!-- * [Reflexión de ondas (No me salen de Ricardo Cabrera)](https://ricuti.com.ar/no_me_salen/ondas/Ap_luz_rerfac.html) -->

Fuente: Phyphox