---
title: "Presión en bolsa de aire"
subtitle: 🛍️🍃📱
math: true
author: Martín Aramayo, Juan Pablo Carbajal
image: "/page/medir-distancias-con-trigonometria/regla-mano-izquierda-portrait.webp"
tags: ["Presión", "Física", "Unidades", "Smartphone", "Phyphox"]
date: 2022-10-15
image: false
--- 

Presión en una superficie, investigar como la presión depende de la fuerza y el área.

<!--more-->

- - -
* **Dificultad:** ★★★★☆

* **Materiales:** 
    * Libros
    * Smartphone (con la app [Phyphox](https://phyphox.org/) y sensor de presión) 
    * Otro dispositivo (Pc, tablet, smartphone)
    * Bolsa de plástico
    * Pesos: 2 x 1 kg y 1 x 0.5 kg
    * Algún objeto chato de 10 cm x 10 cm como una baldosa
    * Banda elástica

* **Duración de preparación:** 30 min

* **Temas:** Presión, experimento práctico.
- - -

La gente de Phyphox armo estos experimentos, los hacemos disponibles traducidos. Dejo link al [original](https://phyphox.org/experiment/pressure-in-a-bag/) con los [ejercicios](https://phyphox.org/material/pressure-in-a-bag.pdf).


### Experimento 1

Examinar el cambio en presión con la fuerza efectiva.
Activar el experimento ``Presión`` en la app de Phyphox.

![](/img/presion-bolsa-aire/ingresarPresion.png)

Para poder controlar el experimento desde otro dispositivo, activar el acceso remoto en los tres puntitos en la esquina superior derecha.
Aparecerá una dirección de internet. Abrir esta dirección en el segundo dispositivo para controlar el experimento de forma remota.

![](/img/presion-bolsa-aire/ingresarPresion2.png)

Colocá el teléfono en la bolsa plástica. Ponele un poco de aire y cerrala bien con una banda elástica. Colocar un libro sobre la bolsa y anotar los valores. Ahora, coloca los pesos, uno tras otro y anotar los cambios en presión.

![](/img/image.webp)

| Masa (Kg)     | Fuerza adicional (N) | Presión (hPa) (Redondeado a 1 dígito) | Cambio en presión respecto a la inicial (hPa)    |
|---------------|----------------------|---------------------------------------|--------------------------------------------------|
| Solo el libro | 0                    |                                       | 0                                                |
| 0.5           | 5                    |                                       |                                                  |
| 1             |                      |                                       |                                                  |
|     _         |                      |                                       |                                                  |
|     _         |                      |                                       |                                                  |
|     _         |                      |                                       |                                                  |
|     _         |                      |                                       |                                                  |

Dibujar el diagrama que muestra el cambio en presión como una función de la fuerza.

### Experimento 2: Presión vs. Área.

Repetir el experimento con una masa de 1 kg y el libro balanceado arriba, podés tocarlo un poquito para que no caiga. 


| Masa (Kg)    | Presión (hPa) (Redondeado a 1 dígito) |
|--------------|---------------------------------------|
| Libro + 1 Kg |                                       |
| 1 Kg + Libro |                                       |


#### Definición de presión:

La presión \\(p\\) está expresada en función de la fuerza que se ejerce sobre un objeto \\(F\\)
con una superficie plana de área \\(A\\):

$$
P = {F \over A}
$$

### Experimento 3: La unidad Pascal

Medir la presión ejercida por una fuerza de 100 N sobre un área de 100 cm\\(^2\\). Además, usar el peso de 1 kg y la "chapita" de 100 cm\\(^2\\).

| Masa (Kg) | Presión (hPa) (Redondeado a 1 dígito) | Cambio en presión |
|-----------|-------------------------------------  |-----------------  |
| Solo chapita |                                       |                   |
| Chapita + 1Kg|                                       |                   |

Ahora sabemos qué presión es ejercida por una fuerza de 10 N sobre un área de 1 dm\\(^2\\). Como 1 m\\(^2\\) es 
100 veces más grande, la fuerza sobre 1 m\\(^2\\) debe ser también 100 veces más grande, i.e. 1000 N, para generar la misma presión. De acuerdo a tu medición, es:
 
$$
P = {F \over A} 
= {10 \text{ N} \over 1 \text{ dm}^2}
= {1000 \text{ N} \over 1 \text{ m}^2}
= \dots \text{hPa}
= \dots \text{Pa}
$$

Por lo tanto, sería

$$
1 \text{Pa}
= {\dots \text{ N} \over \dots \text{ m}^2}
$$

Compara tu medición con la definición de un Pascal de un libro de física o internet. 
Podés encontrar el valor \\(
1 \text{Pa}
= {\dots \text{ N} \over \dots \text{ m}^2}
\\)

Acá va una foto de un experimento:

![](/img/fotitoBolsa.webp)

¿Se te ocurre otro experimento con estos elementos?

### Recomendaciones para aprender más

* [Presión (Hyperphysics)](http://hyperphysics.phy-astr.gsu.edu/hbasees/press.html#pre)
* [Presión (No me salen de Ricardo Cabrera)](https://ricuti.com.ar/no_me_salen/hidrostatica/FT_presion.html)

CC BY-SA Holger Jessen-Thiesen (paper original)