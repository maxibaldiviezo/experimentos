---
title: "Efecto doppler"
subtitle: 🏃📢🧍
math: true
author: Martín Aramayo, Alberto Villagran
image: "/page/medir-distancias-con-trigonometria/regla-mano-izquierda-portrait.webp"
tags: ["Sonido", "Ondas", "Doppler", "Smartphone", "Phyphox"]
date: 2022-10-19
image: false
--- 

Medir el corrimiento de frecuencias, como los que detectan radares policiales o astrónomos con la luz de galaxias.

<!--more-->

- - -
* **Dificultad:** ★★☆☆☆

* **Materiales:** 
    * 2 Smartphones (con la app [Phyphox](https://phyphox.org/)) 
    * Una cinta métrica
    * Un estudiante deportista
    * Un estudiante no deportista
    * Opcional: Parlante bluetooth

* **Duración de preparación:** 40 min

* **Temas:** Sonido, cinemática.
- - -

La gente de Phyphox armó estos experimentos, los hacemos disponibles traducidos. Dejo link al [original](https://phyphox.org/wiki/index.php/Experiment:_Doppler_Effect) con los [ejercicios](http://opentp.fr/en/smart/doppler-effect/). Las imágenes pertenecen a [opentp](www.opentp.fr)
 
![Fuente: www.opentp.fr](/img/doppler.jpg)


### El experimento

El estudiante deportista corre con su smartphone o parlante, emitiendo un armónico puro de tono alto, arriba de los 5000 Hz - para poder diferenciarlo bien del ruido del ambiente. Podés crear un tono puro usando Phyphox (ver imagen). 

![generar tonos](/img/generarTonos.png)

El segundo estudiante se mantiene quieto y mide la frecuencia con su smartphone. El efecto doppler explica que la frecuencia recibida sea diferente a la emitida. Esto depende de la velocidad del corredor y si se acerca o aleja. 

**Desafío:** Determinar la velocidad del corredor con la fórmula del efecto doppler y comparar el resultado con una técnica tradicional de medición de velocidad. Tomando el tiempo transcurrido y distancia cubierta.

<!-- ![generar tonos](/img/efectoDoppler.png) -->

### El efecto doppler

Para eventos de efecto doppler, aplica la siguiente fórmula (siempre que estemos por debajo de la velocidad del sonido, si tu amigo es flash toca usar otra fórmula):

$$
f = {f_0 \over 1 \pm {v \over c}}
$$


\\(f_0\\): Es la frecuencia emitida.

\\(v\\): Es la rapidez con la que el atleta se acerca (-) o aleja (+).

\\(c\\): Es la velocidad del sonido (unos 340 \\(m \over s\\)).

\\(f\\): Es la frecuencia detectada por el segundo estudiante (el que no corre).

### EL DESAFÍO DEFINITIVO

Determina de forma experimental como cambia la frecuencia cuando el segundo estudiante también corre.

### Recomendaciones para aprender más

* [Efecto doppler (Hyperphysics)](http://hyperphysics.phy-astr.gsu.edu/hbasees/Sound/dopp.html#c1)
* [Efecto doppler (No me salen de Ricardo Cabrera)](https://ricuti.com.ar/no_me_salen/ondas/Ap_ond_15.html)


Fuente: [opentp](www.opentp.fr)